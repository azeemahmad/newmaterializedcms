<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    protected function guard()
    {
        return Auth::guard('web');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }
    public function lock(){
        return view('auth.lock');
    }

    public function unlock(Request $request){
        $this->validate($request, [
            'email' => 'required|email',
            'password'=>'required',
        ]);
        $user = User::where('email',$request->email)->first();
        if(!$user){
            session()->flash('error_message','Email Id is not registered with us');
            return redirect('/lock');
        }else if (!Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password])){
            session()->flash('error_message','Incorrect Password');
            return redirect('/lock');
        }else{
            return redirect('/home');
        }
    }

}
