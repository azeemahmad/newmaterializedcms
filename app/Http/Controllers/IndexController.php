<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class IndexController extends Controller
{

    protected function guard()
    {
        return Auth::guard('web');
    }

    public function lock(){
        if(Auth::check()){
            Session::put('adminname',Auth::user()->name);
            Session::put('adminemail',Auth::user()->email);
            Auth::logout();
        }
        return view('auth.lock');
    }

    public function unlock(Request $request){
        $this->validate($request, [
            'email' => 'required|email|exists:users',
            'password'=>'required',
        ]);
        $user = User::where('email',$request->email)->first();
        if(!$user){
            session()->flash('error_message','Email Id is not registered with us');
            return redirect('/lock');
        }else if (!Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password])){
            session()->flash('error_message','Incorrect Password');
            return redirect('/lock');
        }else{
            session()->forget('adminname');
            session()->forget('adminemail');
            $this->guard()->login($user);
            return redirect('/home');
        }
    }

}
