<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <meta name="description" content="Materialize is a Material Design Admin Template,It's modern, responsive and based on Material Design by Google. ">
    <meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
    <title>Materialize - Material Design Admin Template</title>

    <!-- Favicons-->
    <link rel="icon" href="{{asset('images/favicon/favicon-32x32.png')}}" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="{{asset('images/favicon/apple-touch-icon-152x152.png')}}">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="{{asset('images/favicon/mstile-144x144.png')}}">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->

    <link href="{{asset('css/materialize.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{asset('css/style.min.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->
    <link href="{{asset('css/custom/custom-style.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{asset('css/layouts/page-center.css')}}" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="{{asset('js/plugins/prism/prism.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="{{asset('js/plugins/perfect-scrollbar/perfect-scrollbar.css')}}" type="text/css" rel="stylesheet" media="screen,projection">
<style>
    .invalid-feedback{
        color: red !important;
    }
    .alert {
        padding: 15px;
        margin-bottom: 20px;
        border: 1px solid transparent;
        border-radius: 4px;
        color: white;
        font-size: 14px;
    }
    .alert-success{
        background-color: green;
    }
    .alert-danger{
        background-color: red;
    }
</style>
</head>

<body class="cyan">
<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
@if (session('flash_message'))
    <span class="alert alert-success">
                {{ session('flash_message') }}
                     </span>
@endif
@if (session('error_message'))
    <span class="alert alert-danger">
                {{ session('error_message') }}
            </span>
    @endif
@if (session('status'))
    <div class="alert alert-success" role="alert">
        {{ session('status') }}
    </div>
    @endif

            @yield('content')
                    <!-- ================================================
  Scripts
  ================================================ -->

            <!-- jQuery Library -->
            <script type="text/javascript" src="{{asset('js/plugins/jquery-1.11.2.min.js')}}"></script>
            <!--materialize js-->
            <script type="text/javascript" src="{{asset('js/materialize.min.js')}}"></script>
            <!--prism-->
            <script type="text/javascript" src="{{asset('js/plugins/prism/prism.js')}}"></script>
            <!--scrollbar-->
            <script type="text/javascript" src="{{asset('js/plugins/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>

            <!--plugins.js - Some Specific JS codes for Plugin Settings-->
            <script type="text/javascript" src="{{asset('js/plugins.min.js')}}"></script>
            <!--custom-script.js - Add your own theme custom JS-->
            <script type="text/javascript" src="{{asset('js/custom-script.js')}}"></script>

</body>

</html>