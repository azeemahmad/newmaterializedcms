<style>
    footer.page-footer {
        position: absolute;
        width: 100%;
        bottom: 0px;
        right: 0;
    }
</style>
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            <span>Copyright © {{date('Y')}} <a class="grey-text text-lighten-4" href="http://themeforest.net/user/geekslabs/portfolio?ref=geekslabs" target="_blank">GeeksLabs</a> All rights reserved.</span>
            <span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="http://geekslabs.com/">GeeksLabs</a></span>
        </div>
    </div>
</footer>