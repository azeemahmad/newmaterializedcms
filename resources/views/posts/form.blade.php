<div class="row">
    <div class="input-field col s12">
        <input class="" name="title" type="text" id="title" value="{{ isset($post->title) ? $post->title : ''}}" >
        <label for="title" class="">{{ 'Title' }}</label>
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="input-field col s12">

        <textarea class="materialize-textarea" rows="5" name="content" type="textarea" id="content" >{{ isset($post->content) ? $post->content : ''}}</textarea>
        <label for="content" class="">{{ 'Content' }}</label>
        {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <select name="category" class="" id="category" >
            @foreach (json_decode('{"technology": "Technology", "tips": "Tips", "health": "Health"}', true) as $optionKey => $optionValue)
                <option value="{{ $optionKey }}" {{ (isset($post->category) && $post->category == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
            @endforeach
        </select>
        <label for="category" class="">{{ 'Category' }}</label>
        {!! $errors->first('category', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <button class="btn cyan waves-effect waves-light right" type="submit" name="action">{{ $formMode === 'edit' ? 'Update' : 'Create' }}
            <i class="mdi-content-send right"></i>
        </button>
    </div>
</div>