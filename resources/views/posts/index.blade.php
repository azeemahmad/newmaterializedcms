@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="">
            <div class="col-md-9">
                <div class="card">
                    <div class="col s12 m8 l9">
                        <nav class="light  cyan darken-2">
                            <div class="nav-wrapper">
                                <div class="col s12">
                                    <span class="brand-logo">Posts</span>
                                </div>
                            </div>
                        </nav>
                    </div><br/>
                    <div class="card-body">

                            <a href="{{ url('/posts/create') }}" class="btn waves-effect waves-light light-green darken-4  right-align"><i class="mdi-content-add"></i> Add New</a>
                            <div class="row" style="float: right!important;">
                                {!! Form::open(['method' => 'GET', 'url' => 'posts', 'class' => 'navbar-right searchBar', 'role' => 'search'])  !!}

                                <span class="row col s8">
                                   <input type="text" name="search" placeholder="Search..."
                                          style="border: ridge;height: 2rem;">
                               </span>
                                <span class="btn waves-effect waves-light teal searchformsubmit"> <i class="mdi-action-search"></i></span>
                                {!! Form::close() !!}
                            </div>
                            <br/>
                            <br/>

                        <div class="responsive-table">
                            <table class="table striped bordered centered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Content</th>
                                    <th>Category</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->title }}</td>
                                        <td>{{ $item->content }}</td>
                                        <td>{{ $item->category }}</td>
                                        <td>
                                            <a href="{{ url('/posts/' . $item->id) }}" title="View Post">
                                                <button class="btn waves-effect waves-light lime darken-4"><i class="mdi-image-remove-red-eye"></i> View
                                                </button>
                                            </a>
                                            <a href="{{ url('/posts/' . $item->id . '/edit') }}" title="Edit Post">
                                                <button class="btn waves-effect waves-light yellow darken-4"><i class="mdi-editor-border-color"></i> Edit
                                                </button>
                                            </a>

                                            <form method="POST" action="{{ url('/posts' . '/' . $item->id) }}"
                                                  accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn waves-effect waves-light red darken-4" title="Delete Post"
                                                        onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="mdi-action-delete"></i> Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination"> {!! $posts->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.searchformsubmit').click(function(){
            $(this).parents('form').submit();
        });

    </script>
    @endsection
