@extends('layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col s12">
                <nav class="light  cyan darken-2">
                    <div class="nav-wrapper">
                        <div class="col s12">
                            <span class="brand-logo">Post #{{ $post->id }}</span>
                        </div>
                    </div>
                </nav>
            </div>
            <br/>

            <div class="col s6 m4 l6">
                <div class="card">
                    <div class="row" style="margin-left: 0.25rem;">

                        <a href="{{ url('/posts') }}" title="Back" class="btn waves-effect waves-light teal"><i
                                    class="mdi-navigation-arrow-back"></i> Back</a>
                        <a href="{{ url('/posts/' . $post->id . '/edit') }}" title="Edit Post">
                            <button class="btn waves-effect waves-light yellow darken-4"><i
                                        class="mdi-editor-border-color"></i>
                                Edit
                            </button>
                        </a>

                        <form method="POST" action="{{ url('/posts' . '/' . $post->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn waves-effect waves-light red darken-4" title="Delete Post"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i
                                        class="mdi-action-delete"></i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>
                        </div>

                    <div class="responsive-table">
                        <table class="table striped bordered centered">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $post->id }}</td>
                                </tr>
                                <tr>
                                    <th> Title</th>
                                    <td> {{ $post->title }} </td>
                                </tr>
                                <tr>
                                    <th> Content</th>
                                    <td> {{ $post->content }} </td>
                                </tr>
                                <tr>
                                    <th> Category</th>
                                    <td> {{ $post->category }} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
